import ROOT
import os

ROOT.gROOT.SetBatch(True)

AtlasStylePath = os.environ['HOME']+'/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasStyle.C")
  ROOT.gROOT.LoadMacro("common/AtlasLabels.C")
  ROOT.gROOT.LoadMacro("common/AtlasUtils.C")
  print "Set ATLAS Style"
  ROOT.SetAtlasStyle()

from common.ratioplot import ratioplot
histograms = ratioplot()

f = ROOT.TFile(
    "/afs/f9.ijs.si/home/miham/analysis/Hmumu/run2/data.root"
)

suffix = "WZqqll"
# suffix = "JZ2W"

label = {
'WZqqll' : 'PowhegPy8EG_WZqqll_mll20',
'JZ2W' : 'Pythia8EvtGen_jetjet_JZ2W',
}

plots = [
{'branches' : ('recoJ1pt','truthJ1pt','smearedJ1pt'),
 'bins'     : (100, 0, 200, 1000.),
 'xaxis'    : 'p^{inc.}_{T} [GeV]' if suffix == 'JZ2W' else 'p^{1}_{T} [GeV]',
 'name'     : 'pTlead',
 'logy'     : True,
 'suffix'   : ['JZ2W', 'WZqqll'],
},
{'branches' : ('recoJ1qpt','truthJ1qpt','smearedJ1qpt'),
 'bins'     : (100, 0, 200, 1000.),
 'xaxis'    : 'p^{q}_{T} [GeV]',
 'name'     : 'pTlead_q',
 'logy'     : True,
 'suffix'   : ['JZ2W'],
},
{'branches' : ('recoJ1gpt','truthJ1gpt','smearedJ1gpt'),
 'bins'     : (100, 0, 200, 1000.),
 'xaxis'    : 'p^{g}_{T} [GeV]',
 'name'     : 'pTlead_g',
 'logy'     : True,
 'suffix'   : ['JZ2W'],
},
{'branches' : ('recoJ2pt','truthJ2pt','smearedJ2pt'),
 'bins'     : (100, 0, 160, 1000.),
 'xaxis'    : 'p^{2}_{T} [GeV]',
 'name'     : 'pTsublead',
 'logy'     : True,
 'suffix'   : ['WZqqll'],
},
# {'branches' : ('recoJ1eta','truthJ1eta','smearedJ1eta'),
#  'bins'     : (100, -5, 5),
#  'xaxis'    : '#eta^{1}',
#  'name'     : 'etalead',
#  'logy'     : False,
#  'suffix'   : ['JZ2W', 'WZqqll'],
# },
# {'branches' : ('recoJ2eta','truthJ2eta','smearedJ2eta'),
#  'bins'     : (100, -5, 5),
#  'xaxis'    : '#eta^{1}',
#  'name'     : 'etasublead',
#  'logy'     : False,
#  'suffix'   : ['WZqqll'],
# },
{'branches' : ('recoMjj','truthMjj','smearedMjj','partonMjj'),
 'bins'     : (100, 40, 140, 1000.),
 'xaxis'    : 'm(jj) [GeV]',
 'name'     : 'mjj',
 'logy'     : True,
 'suffix'   : ['WZqqll'],
},
]
 
for plot in plots:
    
    if not suffix in plot['suffix']:
        continue

    NBINS, XMIN, XMAX = plot['bins'][0:3]
    SCALE = 1 if len(plot['bins']) == 3 else plot['bins'][3]

    reco = ROOT.TH1D(plot['branches'][0],plot['branches'][0],NBINS, XMIN, XMAX)
    truth = ROOT.TH1D(plot['branches'][1],plot['branches'][1],NBINS, XMIN, XMAX)
    smeared = ROOT.TH1D(plot['branches'][2],plot['branches'][2],NBINS, XMIN, XMAX)
    
    parton = None
    if len(plot['branches']) > 3:
        parton = ROOT.TH1D(plot['branches'][3],plot['branches'][3],NBINS, XMIN, XMAX)

    t = f.Get("event")

    for e in t:
        reco.Fill(getattr(e, plot['branches'][0]) / SCALE)
        truth.Fill(getattr(e, plot['branches'][1]) / SCALE)
        smeared.Fill(getattr(e, plot['branches'][2]) / SCALE)
        if parton:
            parton.Fill(getattr(e, plot['branches'][3]) / SCALE)

    reco.SetLineColor(ROOT.kBlack)
    truth.SetLineColor(ROOT.kRed)
    smeared.SetLineColor(ROOT.kBlue)
    if parton:
        parton.SetLineColor(ROOT.kGreen-2)

    leg = histograms.getLegend(len(plot['branches']), size = 0.06)
    leg.AddEntry(truth,"#font[42]{truth jets}","l")
    leg.AddEntry(reco,"#font[42]{reco jets}","l")
    leg.AddEntry(smeared,"#font[42]{smeared truth}","l")
    if parton:
        leg.AddEntry(parton,"#font[42]{parton level}","l")

    hs = ROOT.THStack()
    if parton:
        hs.Add(parton)
    hs.Add(truth)
    hs.Add(reco)
    hs.Add(smeared)

    hsr = ROOT.THStack()
    truth_r = truth.Clone()
    smeared_r = smeared.Clone()
    truth_r.Divide(reco)
    smeared_r.Divide(reco)
    hsr.Add(truth_r)
    hsr.Add(smeared_r)
    if parton:
        parton_r = parton.Clone()
        parton_r.Divide(reco)
        hsr.Add(parton_r)

    canv, pad1, pad2 = histograms.getCanvas(plot['name'], True, nology=True)

    pad1.cd()
    hs.Draw("nostack hist")
    hs.SetMaximum(truth.GetMaximum()*1.2)
    if AtlasStyle:
        ROOT.ATLASLabel(0.18,0.87,"Simulation Internal",1, 0.06)
        ROOT.myText(0.18,0.81,1,label[suffix], 0.06)
    leg.Draw()
    histograms.configureUpperPad(hs, "Entries")

    pad2.cd()
    hsr.Draw("nostack hist")
    histograms.configureLowerPad(hsr, 0.5, 0.5, plot['xaxis'], "X / reco")

    canv.Print("%s%s.pdf" % (("" if suffix=="" else suffix+"_"), plot['name']))
    if plot['logy']:
        pad1.cd()
        pad1.SetLogy()
        hs.SetMaximum(hs.GetStack().Last().GetMaximum()*10)
        canv.Print("%s%s_LOG.pdf" % (("" if suffix=="" else suffix+"_"), plot['name']))

