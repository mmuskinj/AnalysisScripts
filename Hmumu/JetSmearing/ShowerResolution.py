import ROOT
import os

ROOT.gROOT.SetBatch(True)

AtlasStylePath = os.environ['HOME']+'/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasStyle.C")
  ROOT.gROOT.LoadMacro("common/AtlasLabels.C")
  ROOT.gROOT.LoadMacro("common/AtlasUtils.C")
  print "Set ATLAS Style"
  ROOT.SetAtlasStyle()

f = ROOT.TFile(
    "/afs/f9.ijs.si/home/miham/analysis/Hmumu/run2/data.root"
)

pt_bins = ROOT.TH1D("pt_bins", "pt_bins", 8, 20000., 100000.)
eta_bins = ROOT.TH1D("eta_bins", "eta_bins", 9, 0, 4.5)

param1 = ROOT.TH2D("param1", "param1", 8, 20000., 100000., 9, 0, 4.5)
param2 = ROOT.TH2D("param2", "param2", 8, 20000., 100000., 9, 0, 4.5)
param3 = ROOT.TH2D("param3", "param3", 8, 20000., 100000., 9, 0, 4.5)
param4 = ROOT.TH2D("param4", "param4", 8, 20000., 100000., 9, 0, 4.5)
param5 = ROOT.TH2D("param5", "param5", 8, 20000., 100000., 9, 0, 4.5)

histos_pt = {}
for i in range(0, pt_bins.GetNbinsX()+2):
    for j in range(0, eta_bins.GetNbinsX()+2):
        name = "res_pt_%s_%s" % (i, j)
        histos_pt[i, j] = ROOT.TH1D(name, name, 100, 0, 2)
        
histos_eta = {}
for i in range(0, pt_bins.GetNbinsX()+2):
    for j in range(0, eta_bins.GetNbinsX()+2):
        name = "res_eta_%s_%s" % (i, j)
        histos_eta[i, j] = ROOT.TH1D(name, name, 70, -0.7, 0.7)
        
histos_phi = {}
for i in range(0, pt_bins.GetNbinsX()+2):
    for j in range(0, eta_bins.GetNbinsX()+2):
        name = "res_phi_%s_%s" % (i, j)
        histos_phi[i, j] = ROOT.TH1D(name, name, 70, -0.7, 0.7)

t = f.Get("event")
for i, e in enumerate(t):
    if not i%10000:
        print "processed events: ", i
    parton1pt = e.parton1pt
    parton2pt = e.parton2pt
    parton1eta = e.parton1eta
    parton2eta = e.parton2eta
    parton1etaAbs = abs(e.parton1eta)
    parton2etaAbs = abs(e.parton2eta)
    parton1phi = e.parton1phi
    parton2phi = e.parton2phi
    truthJ1pt = e.truthJ1pt
    truthJ2pt = e.truthJ2pt
    truthJ1eta = e.truthJ1eta
    truthJ2eta = e.truthJ2eta
    truthJ1phi = e.truthJ1phi
    truthJ2phi = e.truthJ2phi
    
    r1 = 1 + (truthJ1pt - parton1pt)/parton1pt
    r2 = 1 + (truthJ2pt - parton2pt)/parton2pt
    histos_pt[pt_bins.FindBin(parton1pt), eta_bins.FindBin(parton1etaAbs)].Fill(r1)
    histos_pt[pt_bins.FindBin(parton1pt), eta_bins.FindBin(parton2etaAbs)].Fill(r2)
    
    dEta1 = parton1eta - truthJ1eta
    dEta2 = parton2eta - truthJ2eta
    histos_eta[pt_bins.FindBin(parton1pt), eta_bins.FindBin(parton1etaAbs)].Fill(dEta1)
    histos_eta[pt_bins.FindBin(parton1pt), eta_bins.FindBin(parton2etaAbs)].Fill(dEta2)
    
    dPhi1 = ROOT.TVector2.Phi_mpi_pi(parton1phi - truthJ1phi)
    dPhi2 = ROOT.TVector2.Phi_mpi_pi(parton2phi - truthJ2phi)
    histos_phi[pt_bins.FindBin(parton1pt), eta_bins.FindBin(parton1etaAbs)].Fill(dPhi1)
    histos_phi[pt_bins.FindBin(parton1pt), eta_bins.FindBin(parton2etaAbs)].Fill(dPhi2)
    
for i in range(2, pt_bins.GetNbinsX()+1):
    for j in range(1, eta_bins.GetNbinsX()+1):
        k = i, j
        
        h1 = histos_pt[k]
        name = "res_pt_%s_%s" % k
        canv = ROOT.TCanvas(name, name, 800, 600)
        h1.Draw()
        #----------
        # fit
        h1.Fit("gaus")
        g = h1.GetFunction("gaus")
        dg = ROOT.TF1("dgaus",
            "[0]*([1]*exp(-(x-[2])^2/2/[3]^2)+(1-[1])*exp(-(x-[2]-[4])^2/2/([3]*[5])^2))", -0, 2)
        dg.SetParameter(0, g.GetParameter(0))
        dg.SetParameter(1, 1)
        dg.SetParLimits(1, .51, 1.)
        dg.SetParameter(2, g.GetParameter(1))
        dg.SetParLimits(2, 0.4, 1.6)
        dg.SetParameter(3, g.GetParameter(2)/2.)
        dg.SetParLimits(3, 0.001, 0.1)
        dg.SetParameter(4, 2)
        dg.SetParLimits(4, -1, 1)
        dg.SetParameter(5, 3.)
        dg.SetParLimits(5, 0, 10.)
        h1.Fit("dgaus");
        param1.SetBinContent(i, j, dg.GetParameter(1))
        param2.SetBinContent(i, j, dg.GetParameter(2))
        param3.SetBinContent(i, j, dg.GetParameter(3))
        param4.SetBinContent(i, j, dg.GetParameter(4))
        param5.SetBinContent(i, j, dg.GetParameter(5))
        # fit
        #----------
        h1.GetXaxis().SetTitle("1 + (p_{T}(truthJet) - p_{T}(parton))/p_{T}(parton)")
        h1.GetYaxis().SetTitle("Entries")
        h1.SetMaximum(h1.GetMaximum()*1.3)
        if AtlasStyle:
            ROOT.ATLASLabel(0.18,0.89,"Simulation Internal",1, 0.045)
            ROOT.myText(0.18,0.83,1,"#sqrt{s}=13 TeV, PowhegPy8EG_WZqqll_mll20", 0.045)
            ROOT.myText(0.18,0.77,1,"parton p_{T} = [%2.0f, %2.0f] GeV" %
                (pt_bins.GetBinLowEdge(k[0])/1000., (pt_bins.GetBinLowEdge(k[0])+pt_bins.GetBinWidth(k[0]))/1000.), 0.045)
            ROOT.myText(0.18,0.71,1,"parton |#eta| = [%1.1f, %1.1f]" %
                (eta_bins.GetBinLowEdge(k[1]), eta_bins.GetBinLowEdge(k[1])+eta_bins.GetBinWidth(k[0])), 0.045)
            #----------
            # fit
            # ROOT.myText(0.6,0.77,1,"#chi^{2}/ndf = %s" % (dg.GetChisquare()), 0.045)
            # fit
            #----------
            if i == 2 and j == 1:
                canv.Print("partonResolution_pt.pdf(")
            elif i == pt_bins.GetNbinsX() and j == eta_bins.GetNbinsX():
                canv.Print("partonResolution_pt.pdf)")
            else:
                canv.Print("partonResolution_pt.pdf")
        h2 = histos_eta[k]
        name = "res_eta_%s_%s" % k
        canv = ROOT.TCanvas(name, name, 800, 600)
        h2.Draw()
        h2.GetXaxis().SetTitle("#eta(parton) - #eta(truthJet)")
        h2.GetYaxis().SetTitle("Entries")
        h2.SetMaximum(h2.GetMaximum()*1.3)
        if AtlasStyle:
            ROOT.ATLASLabel(0.18,0.89,"Simulation Internal",1, 0.045)
            ROOT.myText(0.18,0.83,1,"#sqrt{s}=13 TeV, PowhegPy8EG_WZqqll_mll20", 0.045)
            ROOT.myText(0.18,0.77,1,"parton p_{T} = [%2.0f, %2.0f] GeV" %
                (pt_bins.GetBinLowEdge(k[0])/1000., (pt_bins.GetBinLowEdge(k[0])+pt_bins.GetBinWidth(k[0]))/1000.), 0.045)
            ROOT.myText(0.18,0.71,1,"parton |#eta| = [%1.1f, %1.1f]" %
                (eta_bins.GetBinLowEdge(k[1]), eta_bins.GetBinLowEdge(k[1])+eta_bins.GetBinWidth(k[0])), 0.045)
            if i == 2 and j == 1:
                canv.Print("partonResolution_eta.pdf(")
            elif i == pt_bins.GetNbinsX() and j == eta_bins.GetNbinsX():
                canv.Print("partonResolution_eta.pdf)")
            else:
                canv.Print("partonResolution_eta.pdf")
        h3 = histos_phi[k]
        name = "res_phi_%s_%s" % k
        canv = ROOT.TCanvas(name, name, 800, 600)
        h3.Draw()
        h3.GetXaxis().SetTitle("#phi(parton) - #phi(truthJet)")
        h3.GetYaxis().SetTitle("Entries")
        h3.SetMaximum(h3.GetMaximum()*1.3)
        if AtlasStyle:
            ROOT.ATLASLabel(0.18,0.89,"Simulation Internal",1, 0.045)
            ROOT.myText(0.18,0.83,1,"#sqrt{s}=13 TeV, PowhegPy8EG_WZqqll_mll20", 0.045)
            ROOT.myText(0.18,0.77,1,"parton p_{T} = [%2.0f, %2.0f] GeV" %
                (pt_bins.GetBinLowEdge(k[0])/1000., (pt_bins.GetBinLowEdge(k[0])+pt_bins.GetBinWidth(k[0]))/1000.), 0.045)
            ROOT.myText(0.18,0.71,1,"parton |#eta| = [%1.1f, %1.1f]" %
                (eta_bins.GetBinLowEdge(k[1]), eta_bins.GetBinLowEdge(k[1])+eta_bins.GetBinWidth(k[0])), 0.045)
            if i == 2 and j == 1:
                canv.Print("partonResolution_phi.pdf(")
            elif i == pt_bins.GetNbinsX() and j == eta_bins.GetNbinsX():
                canv.Print("partonResolution_phi.pdf)")
            else:
                canv.Print("partonResolution_phi.pdf")

fOut = ROOT.TFile("Parameterizartion.root","RECREATE")
fOut.cd()                
param1.Write()
param2.Write()
param3.Write()
param4.Write()
param5.Write()
fOut.Close()

