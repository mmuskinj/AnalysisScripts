import ROOT
import os
import random

ROOT.gROOT.SetBatch(True)

AtlasStylePath = os.environ['HOME']+'/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasStyle.C")
  ROOT.gROOT.LoadMacro("common/AtlasLabels.C")
  ROOT.gROOT.LoadMacro("common/AtlasUtils.C")
  print "Set ATLAS Style"
  ROOT.SetAtlasStyle()
  
p0 =  3.99892e+03
p1 =  7.27499e-01
p2 =  9.50552e-01
p3 =  4.49102e-02
p4 = -5.43096e-02
p5 =  2.92745e+00

h = ROOT.TH1D("h", "h", 100, 0, 2)

for _ in range(int(1e5)):
    f = p1
    m = p2
    s = p3
    if random.uniform(0, 1) > f:
        m += p4
        s *= p5
    h.Fill(random.gauss(m, s))
    
f = ROOT.TFile("OutTest.root","RECREATE")
h.Write()
