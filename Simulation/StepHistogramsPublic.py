import ROOT
import os
from math import log10

ROOT.gROOT.SetBatch(True)

AtlasStylePath = os.environ['HOME']+'/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasStyle.C")
  ROOT.gROOT.LoadMacro("common/AtlasLabels.C")
  ROOT.gROOT.LoadMacro("common/AtlasUtils.C")
  print "Set ATLAS Style"
  ROOT.SetAtlasStyle()
  
NEUTRINO_COLOR = ROOT.kBlue

from common.ratioplot import ratioplot
histograms = ratioplot()

f = ROOT.TFile("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/Perf.Nominal.root")
fEM = ROOT.TFile("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/Perf.EM.root")
fNRR = ROOT.TFile("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/Perf.NRR.root")
fEM_NRR = ROOT.TFile("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/Perf.EM.NRR.root")

hNeutrons = f.Get("nSteps/AllATLAS/InitialE/AllATLAS_neutron_InitialE")
hNeutronsNStepsPerE = f.Get("nSteps/AllATLAS/numberOfStepsPerInitialE/AllATLAS_neutron_numberOfStepsPerInitialE")
hNeutronsNRR = fNRR.Get("nSteps/AllATLAS/InitialE/AllATLAS_neutron_InitialE")

hNeutronsNStepsPerE.Divide(hNeutrons)

hElectrons = f.Get("nSteps/AllATLAS/InitialE/AllATLAS_e-_InitialE")
hElectronsNStepsPerE = f.Get("nSteps/AllATLAS/numberOfStepsPerInitialE/AllATLAS_e-_numberOfStepsPerInitialE")
hElectronsEM = fEM.Get("nSteps/AllATLAS/InitialE/AllATLAS_e-_InitialE")

hElectronsNStepsPerE.Divide(hElectrons)

hNeutronsNRR.SetLineColor(NEUTRINO_COLOR)
hNeutronsNStepsPerE.SetLineColor(NEUTRINO_COLOR)
hElectronsEM.SetLineColor(ROOT.kRed+2)
hElectronsNStepsPerE.SetLineColor(ROOT.kRed+2)

print hElectrons.GetNbinsX()
print hElectrons.GetBinWidth(15)
print hElectrons.GetBinWidth(600)

## NRR plot
leg = histograms.getLegend(2, x1 = 0.55, x2 = 0.7, y2 = 0.905, size = 0.06)
leg.AddEntry(hNeutrons,"#font[42]{Default (MC16 production)}","l")
leg.AddEntry(hNeutronsNRR,"#font[42]{#oplus NRR (E = 1 MeV, w = 10)}","l")

hs = ROOT.THStack()
hs.Add(hNeutrons)
hs.Add(hNeutronsNRR)

hsr = ROOT.THStack()
hNeutronsNRR_r = hNeutronsNRR.Clone()
hNeutronsNRR_r.Divide(hNeutrons)
hsr.Add(hNeutronsNRR_r)

canv, pad1, pad2 = histograms.getCanvas("canv1", True, nology=True)
pad1.cd()
hs.Draw("nostack hist")
hs.SetMaximum(hs.GetStack().Last().GetMaximum()*0.7)
if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.85,"Simulation Preliminary",1, 0.06)
    ROOT.myText(0.18,0.77,1,"#sqrt{s}=13 TeV, 10k t#bar{t} events", 0.06)
    ROOT.myText(0.18,0.69,1,"Geant4; neutrons only", 0.06)
leg.Draw()
histograms.configureUpperPad(hs, "Neutrons")
hs.GetXaxis().SetLimits(-3, 4)

pad2.cd()
hsr.Draw("nostack hist")
histograms.configureLowerPad(hsr, 1.0, 1.1, "log_{10}( initial kinetic energy of neutrons [MeV] )", "NRR / Default")
hsr.GetXaxis().SetLimits(-3, 4)
canv.Print("InitialE_NRR.pdf")

## EM plot
leg = histograms.getLegend(2, x1 = 0.55, x2 = 0.7, y2 = 0.905, size = 0.06)
leg.AddEntry(hElectrons,"#font[42]{Default (MC16 production)}","l")
leg.AddEntry(hElectronsEM,"#font[42]{#oplus EM Range Cuts}","l")

hs = ROOT.THStack()
hs.Add(hElectrons)
hs.Add(hElectronsEM)

hsr = ROOT.THStack()
hElectronsEM_r = hElectronsEM.Clone()
hElectronsEM_r.Divide(hElectrons)
hsr.Add(hElectronsEM_r)

canv2, pad12, pad22 = histograms.getCanvas("canv2", True, nology=True)
pad12.cd()
hs.Draw("nostack hist")
hs.SetMaximum(hs.GetStack().Last().GetMaximum()*0.8)
if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.85,"Simulation Preliminary",1, 0.06)
    ROOT.myText(0.18,0.77,1,"#sqrt{s}=13 TeV, 10k t#bar{t} events", 0.06)
    ROOT.myText(0.18,0.69,1,"Geant4; electrons only", 0.06)
leg.Draw()
histograms.configureUpperPad(hs, "Electrons")
hs.GetXaxis().SetLimits(-3.5, 2)

## range cuts:
pad12.Update()
ymax = ROOT.gPad.GetUymax()*0.55
ymin = ROOT.gPad.GetUymin()
LINE_STYLE = 2
LINE_COLOR = ROOT.kGray+2
# LAr // EMEC EMB
l1 = ROOT.TLine(log10(82.9692/1000.), ymin, log10(82.9692/1000.), ymax)
l1.SetLineStyle(LINE_STYLE)
l1.SetLineColor(LINE_COLOR)
l1.Draw()
ROOT.myTextAbs(log10(0.85*82.9692/1000.), 1.1*ymax, 1, "LAr (EMEC)", 0.040, 90)
# LAr // HEC
l12 = ROOT.TLine(log10(349.521/1000.), ymin, log10(349.521/1000.), ymax)
l12.SetLineStyle(LINE_STYLE)
l12.SetLineColor(LINE_COLOR)
l12.Draw()
ROOT.myTextAbs(log10(0.85*349.521/1000.), 1.07*ymax, 1, "LAr (HEC)", 0.040, 90)
# LAr // FCAL
l13 = ROOT.TLine(log10(41.2472/1000.), ymin, log10(41.2472/1000.), ymax)
l13.SetLineStyle(LINE_STYLE)
l13.SetLineColor(LINE_COLOR)
l13.Draw()
ROOT.myTextAbs(log10(0.85*41.2472/1000.), 1.073*ymax, 1, "LAr (FCal)", 0.040, 90)
ROOT.myTextAbs(log10(1.7), 0.7*ymax, 1, "Copper", 0.040, 0, 13)
ROOT.myTextAbs(log10(1.7), 0.6*ymax, 1, "Iron", 0.040, 0, 13)
ROOT.myTextAbs(log10(1.7), 0.5*ymax, 1, "Lead", 0.040, 0, 13)
ROOT.myTextAbs(log10(1.7), 0.4*ymax, 1, "Steel", 0.040, 0, 13)
#Lead // EMB / EMECPara
l3 = ROOT.TLine(log10(241.522/1000.), ymin, log10(241.522/1000.), ymax)
l3.SetLineStyle(LINE_STYLE)
l3.SetLineColor(LINE_COLOR)
l3.Draw()
ROOT.myTextAbs(log10(0.85*241.522/1000.), 0.918*ymax, 1, "Lead (EM)          Copper (EMEC)", 0.040, 90)
#Copper // EMEC
l4 = ROOT.TLine(log10(249.775/1000.), ymin, log10(249.775/1000.), ymax)
l4.SetLineStyle(LINE_STYLE)
l4.SetLineColor(LINE_COLOR)
# l4.Draw()
#Iron // DefaultRegionForTheWorld
# box1 = ROOT.TBox(log10(1.2), ymin, log10(1.5), 0.78*ymax)
# box1.SetFillStyle(3325)
# box1.SetFillColor(LINE_COLOR)
# box1.Draw()
l5 = ROOT.TLine(log10(1.35), ymin, log10(1.35), ymax)
l5.SetLineStyle(LINE_STYLE)
l5.SetLineColor(LINE_COLOR)
l5.Draw()
#Glue
l7 = ROOT.TLine(log10(110.767/1000.), ymin, log10(110.767/1000.), ymax)
l7.SetLineStyle(LINE_STYLE)
l7.SetLineColor(LINE_COLOR)
l7.Draw()
ROOT.myTextAbs(log10(0.9*110.767/1000.), 0.5*ymax, 1, "Glue", 0.040, 90)
#Air
l8 = ROOT.TLine(log10(990/1000000.), ymin, log10(990/1000000.), ymax)
l8.SetLineStyle(LINE_STYLE)
l8.SetLineColor(LINE_COLOR)
l8.Draw()
ROOT.myTextAbs(log10(0.85*990/1000000.), ymax, 1, "Air / Vacuum", 0.040, 90)

hElectronsEM.Draw("same")

pad22.cd()
hsr.Draw("nostack hist")
histograms.configureLowerPad(hsr, 1.0, 1.1, "log_{10}( initial kinetic energy of electrons [MeV] )", "EM / Default")
hsr.GetXaxis().SetLimits(-3.5, 2)
canv2.Print("InitialE_EM.pdf")

## avg. number of Steps
hs = ROOT.THStack()
hs.Add(hNeutronsNStepsPerE)
hs.Add(hElectronsNStepsPerE)

leg = histograms.getLegend(2, x1 = 0.6, x2 = 0.8, y2 = 0.93, size = 0.045)
leg.AddEntry(hNeutronsNStepsPerE,"#font[42]{neutrons}","l")
leg.AddEntry(hElectronsNStepsPerE,"#font[42]{electrons}","l")

for i in range(1,hNeutronsNStepsPerE.GetNbinsX()+1):
    hNeutronsNStepsPerE.SetBinContent(i, hNeutronsNStepsPerE.GetBinContent(i)-1)
    hElectronsNStepsPerE.SetBinContent(i, hElectronsNStepsPerE.GetBinContent(i)-1)

canv3 = ROOT.TCanvas("canv3", "canv3", 800, 600)
canv3.cd()
canv3.SetLogy()
hs.Draw("nostack hist")
hs.GetXaxis().SetTitle("log_{10}( initial kinetic energy [MeV] )")
hs.GetYaxis().SetTitle("avg. number of steps per track")
hs.GetXaxis().SetLimits(-6,6)
hs.SetMaximum(1e4)
if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.89,"Simulation Preliminary",1, 0.045)
    ROOT.myText(0.18,0.83,1,"#sqrt{s}=13 TeV, 10k t#bar{t} events", 0.045)
leg.Draw()

canv3.Update()
ymax = ROOT.gPad.GetUymax()*0.58
ymin = ROOT.gPad.GetUymin()

l1 = ROOT.TLine(0, 10**ymin, 0, 10**ymax)
l1.SetLineStyle(2)
l1.SetLineColor(ROOT.kGray+2)
l1.Draw()
l2 = ROOT.TLine(log10(2), 10**ymin, log10(2), 10**ymax)
l2.SetLineStyle(2)
l2.SetLineColor(ROOT.kGray+2)
l2.Draw()
l5 = ROOT.TLine(log10(5), 10**ymin, log10(5), 10**ymax)
l5.SetLineStyle(2)
l5.SetLineColor(ROOT.kGray+2)
l5.Draw()
ROOT.myTextAbs(log10(1.), 2.*10**ymax, 1, "1 MeV", 0.030, 90, 22)
ROOT.myTextAbs(log10(2.), 2.*10**ymax, 1, "2 MeV", 0.030, 90, 22)
ROOT.myTextAbs(log10(5.), 2.*10**ymax, 1, "5 MeV", 0.030, 90, 22)

ROOT.myTextAbs(log10(0.7), 5.2*10**ymax, 1, "Possible NRR thresholds", 0.030, 0, 13)

box = ROOT.TBox(log10(990/1000000.), 10**ymin, log10(1.5), 10**(ymax*0.43))
box.SetLineStyle(0)
box.SetFillStyle(3325)
box.SetFillColor(ROOT.kGray+1)
box.Draw()
ROOT.myTextAbs(-2.7, 10**(0.43*ymax), 1, "EM Range Cuts", 0.030, 0, 11)

# LAr // HEC
# l12 = ROOT.TLine(log10(349.521/1000.), 10**ymin, log10(349.521/1000.), 10**ymax)
# l12.SetLineStyle(LINE_STYLE)
# l12.SetLineColor(LINE_COLOR)
# l12.Draw()
# ROOT.myTextAbs(log10(0.8*349.521/1000.), 10**(0.38*ymax), 1, "LAr (HEC)", 0.030, 90, 12)

#air
l8 = ROOT.TLine(log10(990/1000000.), 10**ymin, log10(990/1000000.), 10**ymax)
l8.SetLineStyle(LINE_STYLE)
l8.SetLineColor(LINE_COLOR)
l8.Draw()
ROOT.myTextAbs(log10(0.8*990/1000000.), 10**(0.55*ymax), 1, "Air / Vacuum", 0.030, 90, 12)

hs.Draw("same hist nostack")


canv3.Print("AvgNumberOfSteps.pdf")

## Avg plot
from G4Debugger import *
from G4DebuggerUtils import *

G4D1 = G4Debugger("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/","Perf.Nominal")
G4D2 = G4Debugger("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/","Perf.EM.NRR")

print G4D1.volumeSummary

plotSummaryRatio(G4D1.volumeSummary,G4D2.volumeSummary, xaxis="Volumes",
v1="Default", v2="New", ratio = 0.5, mergeOther=True, directory="./", name="volumeSummary")
