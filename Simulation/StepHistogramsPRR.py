import ROOT
import os
from math import log10

ROOT.gROOT.SetBatch(True)

AtlasStylePath = os.environ['HOME']+'/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasStyle.C")
  ROOT.gROOT.LoadMacro("common/AtlasLabels.C")
  ROOT.gROOT.LoadMacro("common/AtlasUtils.C")
  print "Set ATLAS Style"
  ROOT.SetAtlasStyle()
  
NEUTRINO_COLOR = ROOT.kBlue

from common.ratioplot import ratioplot
from common.ratioplot import divideHisto
histograms = ratioplot()

f = ROOT.TFile("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/PRR/Perf.PRR0.root")
fPRR1LAr = ROOT.TFile("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/PRR/Perf.PRR0.5.LAr.root")

hGamma = f.Get("nSteps/AllATLAS/InitialE/AllATLAS_gamma_InitialE")
hGammaCum = hGamma.GetCumulative()
hGammaCum.Scale(100./hGammaCum.GetBinContent(hGammaCum.GetNbinsX()))
hGammaPRR = fPRR1LAr.Get("nSteps/AllATLAS/InitialE/AllATLAS_gamma_InitialE")
hElectrons = f.Get("nSteps/AllATLAS/InitialE/AllATLAS_e-_InitialE")
hElectronsCum = hElectrons.GetCumulative()
hElectronsCum.Scale(100./hElectronsCum.GetBinContent(hElectronsCum.GetNbinsX()))
hElectronsPRR = fPRR1LAr.Get("nSteps/AllATLAS/InitialE/AllATLAS_e-_InitialE")

hGammaNStepsPerE = f.Get("nSteps/AllATLAS/numberOfStepsPerInitialE/AllATLAS_gamma_numberOfStepsPerInitialE")
hGammaNStepsPerE.Divide(hGamma)

hElectronsNStepsPerE = f.Get("nSteps/AllATLAS/numberOfStepsPerInitialE/AllATLAS_e-_numberOfStepsPerInitialE")
hElectronsNStepsPerE.Divide(hElectrons)

hGamma.SetLineColor(ROOT.kBlue-2)
hElectrons.SetLineColor(ROOT.kRed-2)
hGamma.SetLineStyle(7)
hElectrons.SetLineStyle(7)

hGammaPRR.SetLineColor(ROOT.kBlue)
hElectronsPRR.SetLineColor(ROOT.kRed)
hGammaNStepsPerE.SetLineColor(ROOT.kBlue)
hElectronsNStepsPerE.SetLineColor(ROOT.kRed)
hGammaCum.SetLineColor(ROOT.kBlue)
hElectronsCum.SetLineColor(ROOT.kRed)

## PRR plot
leg = histograms.getLegend(4, x1 = 0.18, x2 = 0.4, y2 = 0.75, size = 0.06)
leg.AddEntry(hGamma,"#font[42]{Default photons}","l")
leg.AddEntry(hElectrons,"#font[42]{Default electrons}","l")
leg.AddEntry(hGammaPRR,"#font[42]{PRR(1 MeV) photons}","l")
leg.AddEntry(hElectronsPRR,"#font[42]{PRR(1 MeV) electrons}","l")

hs = ROOT.THStack()
hs.Add(hGamma)
hs.Add(hGammaPRR)
hs.Add(hElectrons)
hs.Add(hElectronsPRR)

hsr = ROOT.THStack()
hsr.Add(divideHisto(hGammaPRR, hGamma))
hsr.Add(divideHisto(hElectronsPRR, hElectrons))

canv, pad1, pad2 = histograms.getCanvas("canv1", True, nology=True)
pad1.cd()
hs.Draw("nostack hist")
hs.SetMaximum(hs.GetStack().Last().GetMaximum()*0.2)
if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.85,"Simulation Internal",1, 0.06)
    ROOT.myText(0.18,0.77,1,"#sqrt{s}=13 TeV, 2k t#bar{t} events", 0.06)
leg.Draw()
histograms.configureUpperPad(hs, "Tracks")
hs.GetXaxis().SetLimits(-4, 2)

pad2.cd()
hsr.Draw("nostack hist")
histograms.configureLowerPad(hsr, 1.0, 1.1, "log_{10}( initial kinetic energy [MeV] )", "PRR / Default")
hsr.GetXaxis().SetLimits(-4, 2)
canv.Print("InitialE_PRR.pdf")

## avg. number of Steps
hs = ROOT.THStack()
hs.Add(hGammaNStepsPerE)
hs.Add(hElectronsNStepsPerE)

leg = histograms.getLegend(2, x1 = 0.6, x2 = 0.8, y2 = 0.93, size = 0.045)
leg.AddEntry(hGammaNStepsPerE,"#font[42]{gamma}","l")
leg.AddEntry(hElectronsNStepsPerE,"#font[42]{electrons}","l")

for i in range(1,hGammaNStepsPerE.GetNbinsX()+1):
    hGammaNStepsPerE.SetBinContent(i, hGammaNStepsPerE.GetBinContent(i)-1)
    hElectronsNStepsPerE.SetBinContent(i, hElectronsNStepsPerE.GetBinContent(i)-1)

canv3 = ROOT.TCanvas("canv3", "canv3", 800, 600)
canv3.cd()
canv3.SetLogy()
hs.Draw("nostack hist")
hs.GetXaxis().SetTitle("log_{10}( initial kinetic energy [MeV] )")
hs.GetYaxis().SetTitle("avg. number of steps per track")
hs.GetXaxis().SetLimits(-6,6)
hs.SetMaximum(1e4)
if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.89,"Simulation Internal",1, 0.045)
    ROOT.myText(0.18,0.83,1,"#sqrt{s}=13 TeV, 2k t#bar{t} events", 0.045)
leg.Draw()

hs.Draw("same hist nostack")

canv3.Update()
ymax = ROOT.gPad.GetUymax()*0.58
ymin = ROOT.gPad.GetUymin()

l1 = ROOT.TLine(0, 10**ymin, 0, 10**ymax)
l1.SetLineStyle(2)
l1.SetLineColor(ROOT.kGray+2)
l1.Draw()
l2 = ROOT.TLine(log10(0.5), 10**ymin, log10(0.5), 10**ymax)
l2.SetLineStyle(2)
l2.SetLineColor(ROOT.kGray+2)
l2.Draw()


canv3.Print("AvgNumberOfStepsPRR.pdf")

## avg. number of Steps
hs = ROOT.THStack()
hs.Add(hGammaCum)
hs.Add(hElectronsCum)

leg = histograms.getLegend(2, x1 = 0.6, x2 = 0.8, y2 = 0.93, size = 0.045)
leg.AddEntry(hGammaNStepsPerE,"#font[42]{gamma}","l")
leg.AddEntry(hElectronsNStepsPerE,"#font[42]{electrons}","l")

canv4 = ROOT.TCanvas("canv4", "canv4", 800, 600)
canv4.cd()
hs.Draw("nostack hist")
hs.GetXaxis().SetTitle("log_{10}( initial kinetic energy [MeV] )")
hs.GetYaxis().SetTitle("Cumulative tracks")
hs.GetXaxis().SetLimits(-4,3)
hs.SetMaximum(130)
if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.89,"Simulation Internal",1, 0.045)
    ROOT.myText(0.18,0.83,1,"#sqrt{s}=13 TeV, 2k t#bar{t} events", 0.045)
leg.Draw()

hs.Draw("same hist nostack")

canv4.Update()
ymax = ROOT.gPad.GetUymax()
ymin = ROOT.gPad.GetUymin()
xmax = ROOT.gPad.GetUxmax()
xmin = ROOT.gPad.GetUxmin()

l1 = ROOT.TLine(0, ymin, 0, ymax)
l1.SetLineStyle(2)
l1.SetLineColor(ROOT.kGray+2)
l1.Draw()
l2 = ROOT.TLine(log10(0.5), ymin, log10(0.5), ymax)
l2.SetLineStyle(2)
l2.SetLineColor(ROOT.kGray+2)
l2.Draw()

ePeak1 = hGammaCum.GetBinContent(hGammaCum.FindBin(log10(0.505)))
ePeak2 = hGammaCum.GetBinContent(hGammaCum.FindBin(log10(0.510)))
l3 = ROOT.TLine(xmin, ePeak1, xmax, ePeak1)
l3.SetLineStyle(2)
l3.SetLineColor(ROOT.kGray+2)
l3.Draw()
l4 = ROOT.TLine(xmin, ePeak2, xmax, ePeak2)
l4.SetLineStyle(2)
l4.SetLineColor(ROOT.kGray+2)
l4.Draw()

ROOT.myTextAbs(-3, ePeak1, ROOT.kGray+2, "%2.1f %s" % (ePeak1, "%"), 0.042, 0, 11)
ROOT.myTextAbs(-3, ePeak2, ROOT.kGray+2, "%2.1f %s" % (ePeak2, "%"), 0.042, 0, 11)


canv4.Print("CumulativeTracksPRR.pdf")


# 
# ## Avg plot
# from G4Debugger import *
# from G4DebuggerUtils import *
# 
# G4D1 = G4Debugger("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/","Perf.Nominal")
# G4D2 = G4Debugger("/afs/f9.ijs.si/home/miham/ceph/Simulation/histograms/public/","Perf.EM.NRR")
# 
# print G4D1.volumeSummary
# 
# plotSummaryRatio(G4D1.volumeSummary,G4D2.volumeSummary, xaxis="Volumes",
# v1="Default", v2="New", ratio = 0.5, mergeOther=True, directory="./", name="volumeSummary")
