import ROOT
ROOT.gROOT.SetBatch(True)

import os

AtlasStylePath = os.environ['HOME'] + '/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)

if AtlasStyle:
    print "setting ATLAS Style"
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasStyle.C")
    ROOT.gROOT.LoadMacro("common/AtlasLabels.C")
    ROOT.gROOT.LoadMacro("common/AtlasUtils.C")
    ROOT.SetAtlasStyle()
    

PRR_track = {
0.5 : [175389.051, 6689.329],
1.0 : [161710.737, 6296.830],
}

PRR_stack = {
0.5 : [169034.677, 6754.567],
1.0 : [148399.101, 5820.453],
}
    
graph0 = {
0   : [180118.020, 7458.596],
}
# 178219.616 +/-   7036.692 sincos_vec

graph1 = {
1   : [167099.646, 6695.689],
2   : [161241.152, 6311.491], # newest one
2.5 : [158283.343, 6490.342],
5   : [155334.374, 6172.183],
7.5 : [151898.495, 6032.739],
10  : [150764.737, 6004.604],
}

graph2 = {
0   : [231232.495, 9167.961],
1   : [206027.939, 8003.510],
2.5 : [213086.152, 8745.862],
5   : [193285.465, 8031.401],
7.5 : [194862.343, 8167.051],
10  : [200102.101, 7994.251],
}

graph3 = {
0   : [166004.566, 6739.854],
1   : [152464.293, 6072.020],
2   : [151616.707, 6076.666], # new
2.5 : [145877.525, 5925.252],
5   : [141156.465, 5755.344],
7.5 : [139882.970, 5628.436],
10  : [139645.687, 5922.180],
}

graph4 = {
0   : [212207.697, 8658.688],
1   : [202395.374, 8202.421],
10  : [175610.081, 7087.191],
}

gr0 = ROOT.TGraphErrors()
for k,v in sorted(graph0.iteritems()):
    if not v: continue
    # gr1.SetPoint(gr1.GetN(), k-0.1, 100.*v[0]/graph1[0][0])
    # gr1.SetPointError(gr1.GetN()-1, 0, 100.*v[1]/graph1[0][0])
    gr0.SetPoint(gr0.GetN(), k-0.01, 100.*v[0]/graph0[0][0])
    gr0.SetPointError(gr0.GetN()-1, 0, 100.*v[1]/graph0[0][0])

gr1 = ROOT.TGraphErrors()
for k,v in sorted(graph1.iteritems()):
    if not v: continue
    # gr1.SetPoint(gr1.GetN(), k-0.1, 100.*v[0]/graph1[0][0])
    # gr1.SetPointError(gr1.GetN()-1, 0, 100.*v[1]/graph1[0][0])
    gr1.SetPoint(gr1.GetN(), k-0.01, 100.*v[0]/graph0[0][0])
    gr1.SetPointError(gr1.GetN()-1, 0, 100.*v[1]/graph0[0][0])

gr2 = ROOT.TGraphErrors()
for k,v in sorted(graph2.iteritems()):
    if not v: continue
    gr2.SetPoint(gr2.GetN(), k+0.05, 100.*v[0]/graph2[0][0])
    gr2.SetPointError(gr2.GetN()-1, 0, 100.*v[1]/graph2[0][0])
    
gr3 = ROOT.TGraphErrors()
for k,v in sorted(graph3.iteritems()):
    if not v: continue
    # gr3.SetPoint(gr3.GetN(), k-0.05, 100.*v[0]/graph1[0][0])
    # gr3.SetPointError(gr3.GetN()-1, 0, 100.*v[1]/graph1[0][0])
    gr3.SetPoint(gr3.GetN(), k+0.01, 100.*v[0]/graph0[0][0])
    gr3.SetPointError(gr3.GetN()-1, 0, 100.*v[1]/graph0[0][0])
    
gr4 = ROOT.TGraphErrors()
for k,v in sorted(graph4.iteritems()):
    if not v: continue
    gr4.SetPoint(gr4.GetN(), k+0.1, 100.*v[0]/graph2[0][0])
    gr4.SetPointError(gr4.GetN()-1, 0, 100.*v[1]/graph2[0][0])
    
MARKER_SIZE = 1.5

NEUTRINO_COLOR = ROOT.kBlue
    
gr0.SetLineColor(ROOT.kBlack)
gr0.SetMarkerColor(ROOT.kBlack)
# gr0.SetMarkerStyle(24)
gr0.SetMarkerSize(MARKER_SIZE)
gr0.SetLineWidth(2)

gr1.SetLineColor(NEUTRINO_COLOR)
gr1.SetMarkerColor(NEUTRINO_COLOR)
gr1.SetMarkerSize(MARKER_SIZE)
gr1.SetLineWidth(2)

gr2.SetLineColor(ROOT.kRed+2)
gr2.SetMarkerColor(ROOT.kRed+2)
gr2.SetMarkerSize(MARKER_SIZE)
gr2.SetLineWidth(2)

gr3.SetLineColor(ROOT.kRed+2)
gr3.SetMarkerColor(ROOT.kRed+2)
gr3.SetMarkerStyle(24)
gr3.SetMarkerSize(MARKER_SIZE)
gr3.SetLineWidth(2)

gr4.SetLineColor(ROOT.kRed+2)
gr4.SetMarkerColor(ROOT.kRed+2)
gr4.SetMarkerStyle(24)
gr4.SetMarkerSize(MARKER_SIZE)
gr4.SetLineWidth(2)

MC16 = 100*186208.960/graph0[0][0]
MC16e = 100*7188.814/graph0[0][0]

gr5 = ROOT.TGraphErrors()
gr5.SetPoint(0, 0+0.03, MC16)
gr5.SetPointError(0, 0, MC16e)
gr5.SetLineColor(ROOT.kOrange+8)
gr5.SetMarkerColor(ROOT.kOrange+8)
gr5.SetMarkerSize(MARKER_SIZE)
# gr5.SetMarkerStyle(24)
gr5.SetLineWidth(2)

grTemp = ROOT.TGraphErrors()
grTemp.SetMarkerStyle(24)
grTemp.SetMarkerSize(MARKER_SIZE)
grTemp.SetLineWidth(2)
    
mg = ROOT.TMultiGraph()
mg.Add(gr0,"p e")
mg.Add(gr1,"p e")
# mg.Add(gr2,"pe")
mg.Add(gr3,"p e")
# mg.Add(gr4,"pe")
mg.Add(gr5,"p e")

leg = ROOT.TLegend(0.60, 0.93-4*0.06, 0.8, 0.93)
leg.SetBorderSize(0)
leg.SetFillColor(0)
leg.SetFillStyle(0)
leg.SetTextSize(0.045)
leg.AddEntry(gr0, "#font[42]{Default, 21.0.93}", "p e x0")
leg.AddEntry(gr1, "#font[42]{#oplus NRR}", "p e x0")
# leg.AddEntry(gr2,"#font[42]{local T3}")
leg.AddEntry(gr3, "#font[42]{#oplus EM range cuts}", "p e x0")
leg.AddEntry(gr5, "#font[42]{AtlasOffline, 21.0.15}", "p e x0")

canv = ROOT.TCanvas("canv","canv",800,600)
mg.Draw("a")
mg.SetMaximum(125)
mg.SetMinimum(70)
mg.GetXaxis().SetLimits(-1,11)
mg.GetXaxis().SetTitle("NRR Energy threshold [MeV]")
mg.GetYaxis().SetTitle("CPU time per event [%]")
if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.89,"Simulation Preliminary",1, 0.045)
    ROOT.myText(0.18,0.83,1,"#sqrt{s}=13 TeV, 100 t#bar{t} events", 0.045)
    ROOT.myText(0.18,0.77,1,"NRR weight = 10", 0.045)
    # ROOT.ATLASLabel(0.18, 0.88, "Simulation Preliminary", 1)
    # ROOT.myText(0.18, 0.82, 1, "#sqrt{s}=13 TeV, 100 t#bar{t} events")
    # ROOT.myText(0.18, 0.76, 1, "NRR weight = 10")
leg.Draw()

l1 = ROOT.TLine(-1, 90, 11, 90)
l1.SetLineStyle(2)
# l1.SetLineColor(ROOT.kGray-1)
l1.Draw()

l2 = ROOT.TLine(-1, 80, 11, 80)
l2.SetLineStyle(2)
# l2.SetLineColor(ROOT.kGray-1)
l2.Draw()

canv.Print("NRRPerformance.pdf")
