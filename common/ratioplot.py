import ROOT

def divideHisto(h1, href):
    hout = h1.Clone()
    hout.Divide(href)
    return hout

class ratioplot:
  rsplit = 0.35
  pad1TopMarin = 0.07
  pad1BotMarin = 0.04
  pad2TopMarin = 0.03
  pad2BotMarin = 0.4
  LeftMargin = 0.15
  yaxis = "Steps"
  
  def getLegend(self, N, x1 = 0.6, x2 = 0.9, y2 = 0.9, size = 0.045):
    leg = ROOT.TLegend(x1, y2 - N*(size*1.3), x2, y2)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(size)
    return leg

  def getCanvas(self, uniqueName, nologx = False, nology = False):
    canv = ROOT.TCanvas(uniqueName,uniqueName,800,600)
    pad1 = ROOT.TPad("pad1"+uniqueName,"top pad"+uniqueName,0.,self.rsplit,1.,1.)
    pad1.SetTopMargin(self.pad1TopMarin)
    pad1.SetBottomMargin(self.pad1BotMarin)
    pad1.SetLeftMargin(self.LeftMargin)
    if not nologx:
      pad1.SetLogx()
    if not nology:
      pad1.SetLogy()
    pad1.Draw()
    pad2 = ROOT.TPad("pad2"+uniqueName,"bottom pad"+uniqueName,0,0,1,self.rsplit)
    pad2.SetTopMargin(self.pad2TopMarin)
    pad2.SetBottomMargin(self.pad2BotMarin)
    pad2.SetLeftMargin(self.LeftMargin)
    pad2.SetTicky()
    pad2.SetTickx()
    if not nologx:
      pad2.SetLogx()
    pad2.SetGridy()
    pad2.Draw()
    return canv, pad1, pad2

  def configureUpperPad(self,hs1, yaxis=""):
    hs1.GetXaxis().SetLabelSize(0)
    hs1.GetYaxis().SetLabelSize(0.062)
    hs1.GetYaxis().SetTitleSize(0.065)
    hs1.GetYaxis().SetTitleOffset(0.95)
    hs1.GetYaxis().SetTitle(self.yaxis if yaxis=="" else yaxis)
    ROOT.gPad.RedrawAxis()

  def configureLowerPad(self, h2, ratioDn, ratioUp, xaxis, yaxisr):
    h2.GetXaxis().SetTitle(xaxis)
    h2.GetYaxis().SetTitle(yaxisr)
    h2.GetXaxis().SetTitleSize(0.13)
    h2.GetXaxis().SetTitleOffset(1.2)
    h2.GetXaxis().SetLabelSize(0.12)
    h2.GetYaxis().SetTitleSize(0.12)
    h2.GetYaxis().SetTitleOffset(0.5)
    h2.GetYaxis().SetLabelSize(0.12)
    h2.GetYaxis().SetLabelOffset(0.015)
    h2.GetYaxis().SetRangeUser(0.5,1.5)
    h2.GetYaxis().SetNdivisions(6)
    h2.SetMinimum(1.0 - ratioDn)
    h2.SetMaximum(1.0 + ratioUp)
    ROOT.gPad.RedrawAxis()
